import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

public class FirePdfBug
{
  public static void main(String[] args) throws MalformedURLException, IOException, DocumentException
  {
    Document.compress = false;

    Image img = Image.getInstance("rgba.png");
    Document doc = new Document(PageSize.LETTER);
    PdfWriter.getInstance(doc, new FileOutputStream("rgba.pdf"));
    doc.open();
    doc.add(img);
    doc.close();

    img = Image.getInstance("indexed.png");
    doc = new Document(PageSize.LETTER);
    PdfWriter.getInstance(doc, new FileOutputStream("indexed.pdf"));
    doc.open();
    doc.add(img);
    doc.close();
  }
}
